#Apresentação

``` 
- type: animation
- mode: sides
```

``` 
- type: video
- url: assets/videos/adp-intro.webm
- autoplay: true
```



#Pesquisa de Requisição de Pessoal

##capa

``` 
- type: animation
- mode: sides
```

``` 
- type: magic-block
- class: adp-cover-logo
- image: logo_adp_entrada.png
- description: <span></span>
- animation: from_left
- position: [0, 70]
- delay: 0.5
- time: 1
```

``` 
- type: magic-block
- class: adp-cover-title
- description:The insights, tools and data you need to unlock your potential. <p>Explore ADP <strong>Insights & Resorces</strong></p>
- animation: from_right
- position: [35, 70]
- delay: 1
- time: 1
```


#Projetando cursos EAD

``` 
- type: animation
- mode: sides
```

###No box Requisição de Pessoal, acesse Requisição de Pessoal.
![alt opcional](imagem5.jpg "tittle")
```
- type: highlight
- size: [200, 37]
- position: [260, 255]
```


#Pesquisa de Requisição de Pessoal
![alt opcional](blur.png "tittle")

``` 
- type: animation
- mode: sides
```

``` 
- type: audio
- audio: bruna-nova-bem-vindo.mp3
```

``` 
- type: magic-block
- class: adp-red-box
- description:The insights, tools and data you need to unlock your potential. <p>Explore ADP <strong>Insights & Resorces</strong></p>
- animation: from_right
- position: [50, 30]
- delay: 0.5
- time: 1
```

``` 
- type: magic-block
- class: avatar
- image: bruna.png
- description:<span></span>Para começar, <strong>abordaremos o passo-a-passo de como pesquisar</strong> as requisições de pessoal.
- animation: from_left
- position: [10, 10]
- delay: 0.5
- time: 1
```


#Pesquisa de Requisição de Pessoal

###No box Requisição de Pessoal, acesse Requisição de Pessoal.

``` 
- type: animation
- mode: sides
```

![alt opcional](imagem5.jpg "tittle")
```
- type: highlight
- size: [200, 37]
- position: [260, 255]
```

``` 
- type: magic-block
- class: adp-black-box
- description:Este curso está organizado da seguinte forma: <br /> <ul><li>Cadastros e Parametrizações</li><li>Gestão de Cargos e Salários</li><li>Relatórios</li></ul>
- animation: from_right
- position: [50, 25]
- delay: 0.5
- time: 1
```


#Pesquisa de Requisição de Pessoal

###Você pode filtrar sua pesquisa de acordo com a situação da requisição de pessoal. Nesse exemplo, altere o campo Situação para Todas.
![alt opcional](imagem8.jpg "tittle")
```
- type: highlight
- size: [170, 27]
- position: [20, 172]
```


#Pesquisa de Requisição de Pessoal

###Você pode filtrar sua pesquisa de acordo com a situação da requisição de pessoal. Nesse exemplo, altere o campo Situação para Todas.
![alt opcional](imagem9.jpg "tittle")

```
- type: highlight
- size: [160, 17]
- position: [20, 195]
```

#Titulo E-learning 8

###Clique no botão Pesquisar.
![alt opcional](imagem10.jpg "tittle")
```
- type: highlight
- size: [83, 25]
- position: [640, 172]
```

#Pesquisa de Requisição de Pessoal

###Pronto, o histórico das Requisições de Pessoal será exibido, conforme Situação escolhida! Antes de prosseguir, confira as funcionalidades destacadas acima.
![alt opcional](imagem11.jpg "tittle")


``` 
- type: tip
- description: Você pode refinar sua filtragem por Empresa e Estabelecimento e/ou Função e/ou Solicitante.
- position: [5, 10]
- animation: scale
- delay: 1.2
- time: 2
```

``` 
- type: tip
- description: Campo utilizado para filtrar pelo tipo da fase.
- position: [15, 10]
- animation: scale
- delay: 1.2
- time: 2
```

``` 
- type: tip
- description: Número de itens que serão exibidos. Limite máximo: 50 itens
- position: [25, 10]
- animation: scale
- delay: 1.2
- time: 2
```
