'use strict';

angular.module('eLearning')
  .controller('NavbarCtrl', function ($scope, Navigate, Sound, localStorage) {
    
    localStorage.localVar("c4-lerning");
    
    var open = true;

    $scope.open = function(){

      if(open == true){
        open = false;
        TweenMax.to(angular.element("#menu"),0.5,{right:0, overwrite:true});
      }else{
        open = true;
        TweenMax.to(angular.element("#menu"),0.5,{right:-350, overwrite:true});
      }

      
    }

    $scope.next = function(){
  		Navigate.change("next");
  	}

  	$scope.prev = function(){
  		Navigate.change("prev");
  	}
    
    $scope.pause = function(){
  		Sound.pause();
  	}
    
    $scope.play = function(){
  		Sound.play();
  	}

    $scope.open_window = function(){
      angular.element(".help").css("display", "block");

      angular.element(".help").css("left", ( ( angular.element(window).width() - angular.element(".help").width() ) / 2 ) + "px");
      angular.element(".help").css("top", ( ( angular.element(window).height()  - angular.element(".help").height() ) / 2 ) + "px");

      TweenMax.from(angular.element(".help"),1,{scale:0,ease:Expo.easeOut});
    }

    $scope.close_window = function(){
      angular.element(".help").css("display", "none");
    }
    
    $scope.nav_top = angular.element(window).height() / 2;
    
    angular.element( window ).resize(function() {
      angular.element(".next-bt").css("top", ( ( angular.element(window).height() / 2 ) - 50 ) + "px");
      angular.element(".prev-bt").css("top", ( ( angular.element(window).height() / 2 ) - 50 ) + "px");

      angular.element(".help").css("left", ( ( angular.element(window).width() - angular.element(".help").width() ) / 2 ) + "px");
      angular.element(".help").css("top", ( ( angular.element(window).height()  - angular.element(".help").height() ) / 2 ) + "px");

    });

    var s = JSON.parse(localStorage.get("md"));

    $scope.title = s[0].title;
    
  });
