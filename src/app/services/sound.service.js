angular.module('eLearning')
  .service('Sound', function(ngAudio) {
    
    var mp3 = "";
    var sound = "";
    
    this.start = function(song){
      mp3 = song;
      
      sound = ngAudio.load("/assets/audio/" + mp3);
    },
    
    this.play = function(){
      if(sound)
        sound.play();
    },
    
    this.pause = function(){
      if(sound)
        sound.pause();
    },
    
    this.stop = function(){
      if(sound)
        sound.stop();
        
      mp3 = ""
      sound = ""
    }
    
  });