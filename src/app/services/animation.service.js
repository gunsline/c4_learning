angular.module('eLearning')
  .service('Animation', function(ngAudio) {
            
    this.go = function(){
      
      var temp_element = angular.element(".slide.selected .e-block .e-animate");
      
      //animacao content block
      if(angular.element(temp_element).length > 0){
        
        for (var i = 0; i < angular.element(temp_element).length; ++i) {
          
          var obj = angular.element(temp_element).eq(i).parent();
          var type = angular.element(temp_element).eq(i).find(".type").html();
          var delay = angular.element(temp_element).eq(i).find(".delay").html();
          var out = angular.element(temp_element).eq(i).find(".go_out").html();        
          var time = angular.element(temp_element).eq(i).find(".time").html();        
          var rotation = angular.element(temp_element).eq(i).find(".rotation").html();   
          var zoom = angular.element(temp_element).eq(i).find(".zoom").html();    
          
          var _width = angular.element(".slide.selected").width();
          var _height = angular.element(".slide.selected").height();
          
          if(rotation == ""){
            rotation = 0;
          }

          if(zoom == ""){
            zoom = 1;
          }
          
          if(delay == ""){
            delay = 0;
          }
          
          if(time == ""){
            time = 1;
          }
          
          if(type == "from_left"){
            TweenMax.from(obj,time,{opacity:0,left:-_width, rotation:rotation, overwrite:true, delay:delay});
          }
          
          if(type == "from_right"){
            TweenMax.from(obj,time,{opacity:0,left:_width, rotation:rotation, overwrite:true, delay:delay});
          }
          
          if(type == "scale"){
            TweenMax.from(obj,time,{opacity:1, scale:0.1, rotation:rotation, overwrite:true, delay:delay,ease:Expo.easeOut});
          }

          if(type == "zoom"){
            TweenMax.to(obj,time,{opacity:1, scale:zoom, overwrite:true, delay:delay,ease:Expo.easeOut});
          }
          
        }
      }
      
      var highlight_element = angular.element(".slide.selected .e-highlight .e-animate");
      
      //animacao highlight
      if(angular.element(highlight_element).length > 0){
        
        for (var i = 0; i < angular.element(highlight_element).length; ++i) {
          
          var highlight_obj = angular.element(highlight_element).eq(i).parent();
          var highlight_type = angular.element(highlight_element).eq(i).find(".type").html();
          var highlight_delay = angular.element(highlight_element).eq(i).find(".delay").html();     
          
          var _width = angular.element(".slide.selected").width();
          var _height = angular.element(".slide.selected").height();
          
          if(highlight_delay == ""){
            highlight_delay = 0;
          }
          
          if(highlight_type == "show"){
            TweenMax.from(highlight_obj,1,{opacity:0,delay:highlight_delay});
          }
        }
      }
      
      var radio_element = angular.element(".slide.selected .e-radio .e-animate");
      
      //animacao radio
      if(angular.element(radio_element).length > 0){
        
        for (var i = 0; i < angular.element(radio_element).length; ++i) {
          
          var radio_obj = angular.element(radio_element).eq(i).parent();
          var radio_type = angular.element(radio_element).eq(i).find(".type").html();
          var radio_delay = angular.element(radio_element).eq(i).find(".delay").html();     
          
          var _width = angular.element(".slide.selected").width();
          var _height = angular.element(".slide.selected").height();
          
          if(radio_delay == ""){
            radio_delay = 0;
          }
          
          if(radio_type == "scale"){
            TweenMax.from(radio_obj,2,{scale:0,delay:radio_delay,ease:Elastic.easeInOut});
          }
        }
      }
      
      //animacao tip
      if(angular.element(".slide.selected .e-tip .e-animate").length > 0){
        for (var i = 0; i < angular.element(".slide.selected .e-tip .e-animate").length; ++i) {
       
          var obj_tip = angular.element(".slide.selected .e-tip .e-animate").eq(i).parent();
          var type_tip = angular.element(".slide.selected .e-tip .e-animate").eq(i).find(".type").html();
          var delay_tip = angular.element(".slide.selected .e-tip .e-animate").eq(i).find(".delay").html();
          var time_tip = angular.element(".slide.selected .e-tip .e-animate").eq(i).find(".time").html();        
          var rotation_tip = angular.element(".slide.selected .e-tip .e-animate").eq(i).find(".rotation").html();       
          
          _width = angular.element(".slide.selected").width();
          _height = angular.element(".slide.selected").height();
          
          if(rotation_tip == ""){
            rotation_tip = 0;
          }
          
          if(delay_tip == ""){
            delay_tip = 0;
          }
          
          if(time_tip == ""){
            time_tip = 1;
          }
          
          if(type_tip == "from_left"){
            TweenMax.from(obj_tip,time_tip,{opacity:0, left:-_width, rotation:rotation_tip, overwrite:true, delay:delay_tip});
          }
          
          if(type_tip == "from_right"){
            TweenMax.from(obj_tip,time_tip,{opacity:0, left:_width, rotation:rotation_tip, overwrite:true, delay:delay_tip});
          }
          
          if(type_tip == "scale"){
            TweenMax.from(obj_tip,time_tip,{opacity:0, scale:0, rotation:rotation_tip, overwrite:true, delay:delay_tip,ease:Elastic.easeInOut});
          }
        }
        
      }
      
    }
    
  });
