angular.module('eLearning')
  .service('Navigate', function(Sound, Animation, localStorage, utils) {
    var currentPage = 0;
    var lastPage = 0;
    
    var slide_in;
    var slide_out;
    
    var slide_off;
    
    this.getPage = function(){
      return currentPage;
    },
    
    this.setPage = function(temp){
      currentPage = parseInt(temp);
      
      lastPage = currentPage - 1;
      
      if(lastPage < 0 ){
        lastPage = 0;
      }
      
      angular.element(".slide").removeClass("last_selected");
      angular.element(".slide").removeClass("selected"); 
      angular.element(".slide").eq(lastPage).addClass("last_selected");
      angular.element(".slide").eq(currentPage).addClass("selected");
      
    },
    
    this.transition = function(page, go){

      Sound.stop();

      if(!utils.nil(go)){
        go = "next";
      }
      
      var temp = angular.element(".slide");

      this.legendUpdate();
      this.titleUpdate();
      
      clearTimeout(slide_off);
        
      angular.element(".slide").removeClass("slide_off");
      
      angular.element(".slide").removeClass("last_selected"); 
      angular.element(".slide").removeClass("selected"); 
      
      //remove todos elementos
      angular.element(".slide").removeClass("slide-content-animate-from-left");
      angular.element(".slide").removeClass("slide-content-animate-out-left");
      
      angular.element(".slide").removeClass("slide-content-animate-from-right");
      angular.element(".slide").removeClass("slide-content-animate-out-right");
      
      angular.element(".slide").removeClass("slide-content-animate-in");
      angular.element(".slide").removeClass("slide-content-animate-out");
      
      angular.element(".slide").removeClass("slide-content-animate-show");
      angular.element(".slide").removeClass("slide-content-animate-hide");
      
      //faz animacoes de saida e entrada
      temp.eq(lastPage).addClass(animation(temp.eq(lastPage).find(".animation div").attr("class"),"out",go));
      temp.eq(currentPage).addClass(animation(temp.eq(currentPage).find(".animation div").attr("class"),"in",go));
      
      temp.eq(lastPage).addClass("last_selected");
      temp.eq(currentPage).addClass("selected");
      
      slide_off = setTimeout(slideOffAction, 700);
      
      Animation.go();
      
      function slideOffAction(){
        angular.element(".last_selected").addClass("slide_off");
      }
      
      if(angular.element(".slide.selected .e-audio").length > 0){
        console.log(angular.element(".slide.selected .e-audio").attr("id"));
     
        Sound.start(angular.element(".slide.selected .e-audio").attr("id"));
        Sound.play();
      } 
      
      if (temp.eq(currentPage)[0]){
        //angular.element(".legend-text p").html(angular.element(".slide.selected p").html());
      }
      
      var zoom_from = temp.eq(currentPage).find(".zoom .from").html();
      var zoom_to = temp.eq(currentPage).find(".zoom .to").html();
      
      TweenMax.fromTo(temp.eq(currentPage),3,{visible:true,scale:zoom_from},{scale:zoom_to,delay:.5,ease:Expo.easeOut});
      
      function animation(temp, side, go){
        var r = "";
        switch(temp) {
          case "sides":
              if(go == "next"){
                if(side == "in"){
                  r = "slide-content-animate-from-left";
                }else if(side == "out"){
                  r = "slide-content-animate-out-left";
                }
              }else{
                if(side == "in"){
                  r = "slide-content-animate-from-right";
                }else if(side == "out"){
                  r = "slide-content-animate-out-right";
                }
              }
              break;
          case "scale-down":
              if(side == "in"){
                r = "slide-content-animate-in";
              }else if(side == "out"){
                r = "slide-content-animate-out";
              }
              break;
          case "alpha":
              if(side == "in"){
                r = "slide-content-animate-show";
              }else if(side == "out"){
                r = "slide-content-animate-hide";
              }
              break;
          default:
              
        } 
        
        return r;
      }
      
      angular.element(".pages").html(( currentPage + 1 ) + "/" + angular.element(".slide").length + "");

    },

    this.goTo = function(page){

      var temp = angular.element(".slide");

      var goOn = false;

      currentPage = page;

      if(currentPage > temp.length - 1){
        currentPage = temp.length - 1;
      }        

      if(currentPage < 0){
        currentPage = 0;
      }

    },

    this.change = function(page){ 

      lastPage = currentPage;

      var temp = angular.element(".slide");

      var goOn = false;

      if(page == "next"){
        currentPage += 1;

        if(currentPage > temp.length - 1){
          currentPage = temp.length - 1;

          goOn = true;
        }        

      }else if(page == "prev"){
        currentPage -= 1;

        if(currentPage < 0){
          currentPage = 0;

          goOn = true;
        }

        
      }

      if(!goOn){
        this.transition(currentPage, page);
      }
    },

    this.legendUpdate = function(){
      
      localStorage.localVar("c4-lerning");

      var s = JSON.parse(localStorage.get("md"));
      angular.element(".legend").addClass("hide");
      angular.element(".legend").removeClass("show");

      console.log(s[currentPage].legend);

      if(utils.nil(s[currentPage].legend)){
        angular.element(".legend").removeClass("hide");
        angular.element(".legend").addClass("show");
        angular.element(".legend-text p").html(s[currentPage].legend);
      }
    },

    this.titleUpdate = function(){
      
      localStorage.localVar("c4-lerning");

      var s = JSON.parse(localStorage.get("md"));
            
      if(utils.nil(s[currentPage].title)){
        angular.element(".head-title p").html(s[currentPage].title);
      }
      
    }
});