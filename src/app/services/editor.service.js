angular.module('eLearning')
  .service('eEditor', function(eMarkdown, localStorage, utils) {

    var current = 0;

    var slides;

    var codemirror; 

    var currentPage = 0;

    //tava dando erro neste inicializador
    try{
      codemirror = CodeMirror.fromTextArea(document.getElementById("e_editor"), {
        mode: 'markdown',
        lineNumbers: true,
        theme: "default",
        extraKeys: {"Enter": "newlineAndIndentContinueMarkdownList"}
      });
    }catch(e){}

    this.setCurrentPage = function(num){
      currentPage = num;

      console.log(currentPage);
    }

    this.setSlides = function(_slides){
      slides = _slides;
    },

    this.getSlides = function(){
      return slides;
    },

    this.getSlide = function(num){
      return slides[num]["etext"];
    },

    this.getVar = function(){
      return codemirror;
    },

    this.start = function(){

    },

    this.init = function(scope){

      //localStorage
      if(!utils.nil(localStorage.get("md")))
      {
        localStorage.set("md",JSON.stringify(scope.slides));
      }

      slides = JSON.parse(localStorage.get("md"));
      
      var $this = this;

      codemirror.on('change', function(c) {
        // change is raised at the beginning with any real change

        if (c.getValue()) {
          
          var s = c.getValue();

          //transforma de markdown para json
          var temp = eMarkdown.init(s);

          slides = JSON.parse(localStorage.get("md"));
          
          slides[currentPage] = temp[0];

          scope.slides = temp;

          $this.legendUpdate();

          $this.titleUpdate();

          localStorage.set("md",JSON.stringify(slides));

          if (scope.$root.$$phase != '$apply' && scope.$root.$$phase != '$digest') {
            scope.$apply();
          }

          
        }
      });  
    },

    this.legendUpdate = function(){
      
      localStorage.localVar("c4-lerning");

      var s = JSON.parse(localStorage.get("md"));
      angular.element(".legend").addClass("hide");
      angular.element(".legend").removeClass("show");

      if(utils.nil(s[currentPage].legend)){
        angular.element(".legend").removeClass("hide");
        angular.element(".legend").addClass("show");
        angular.element(".legend-text p").html(s[currentPage].legend);
      }
      
    },

    this.titleUpdate = function(){
      
      localStorage.localVar("c4-lerning");

      var s = JSON.parse(localStorage.get("md"));
            
      if(utils.nil(s[currentPage].title)){
        angular.element(".head-title p").html(s[currentPage].title);
      }
      
    }
  });