angular.module('eLearning')
  .directive('eVideo', function (Video) {
  
  return function(scope, element, attr){

    Video.setVideo(attr.eVideo);
    scope.video = Video.getVideo(); 
    
  }
  
});

