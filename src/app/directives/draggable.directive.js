angular.module('eLearning')
  .directive('eDraggable', function ($window, localStorage, EditorNavigate, eEditor, utils) {
  
  return function(scope, element, attr){
    
    var w = $window.innerWidth;
    var h = $window.innerHeight;

    Draggable.create(element, {trigger:element.find(".draggble"), 
                               type:"x,y",
                               onDragEnd: function(){calcPercent(this.x,this.y)}, 
                               edgeResistance:0.5, 
                               throwProps:true});


    function calcPercent(x,y) {
      var Xpercent = Math.round(x / w * 100);
      var Ypercent = Math.round(y / h * 100);

      localStorage.localVar("c4-lerning");

      var currentPage = EditorNavigate.getPage();

      var s = JSON.parse(localStorage.get("md"));
      
      var slide = s[currentPage];

      if(attr.class == "e-highlight"){

        slide.code[attr.eCodeIndex].position[0] = slide.code[attr.eCodeIndex].position[0] + x;
        slide.code[attr.eCodeIndex].position[1] = slide.code[attr.eCodeIndex].position[1] + y;
        
        eEditor.getVar().setValue(utils.object_to_md(slide));
        
        return;
      }

      //avatar
      if(attr.class == "e-block avatar"){
        slide.code[attr.eCodeIndex].position[1] = slide.code[attr.eCodeIndex].position[1] - Ypercent;
      }else{
        slide.code[attr.eCodeIndex].position[1] = slide.code[attr.eCodeIndex].position[1] + Ypercent;
      }
      slide.code[attr.eCodeIndex].position[0] = slide.code[attr.eCodeIndex].position[0] + Xpercent;
      eEditor.getVar().setValue(utils.object_to_md(slide));

    };

  }
  
});