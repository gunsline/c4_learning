angular.module('eLearning')
  .directive('eEditorPage', function (localStorage, $timeout, EditorNavigate, eEditor) {
  
  return function(scope, element, attr){
  	
  	var regex = /[0-9]$/;

    localStorage.localVar("c4-lerning");
    
    var slide_s;

    var slide;

    element.bind("keyup", function() {      

      var s = angular.element(element).val();
      
      if(s.replace(/ /g,'').match(regex)){

      	if(s > 0 && s <= eEditor.getSlides().length){

          var currentPage = s - 1;

          slide_s = JSON.parse(localStorage.get("md"));

          slide = slide_s[currentPage];

          EditorNavigate.setPage(currentPage + 1);

      		eEditor.getVar().setValue(slide.etext);
      	}
      }
      
    });    
  }
  
});

