angular.module('eLearning')
  .directive('eInit', function ($timeout, Animation, eEditor) {
  
  return function(scope, element, attr){
    
    setTimeout(function(){
      
      Draggable.create(".e-editor", {trigger:".md-toolbar-tools", type:"top,left", edgeResistance:0.5, throwProps:true});

      Animation.go();

      load_site();

      //slide 5
      angular.element(".e-tab ul").height(angular.element(window).height());
      angular.element(".e-tab .e-tab-content").height(angular.element(window).height());

      function loadImage(url,callback)
      {
        $('<img />')
        .attr('src', url)
        .load(callback);  
      }

      function load_site(){
        
        
        angular.element(".slide.selected").css('display','none');
        angular.element(".loader").css('display','block');

        angular.element(".loader").css("left", ( ( angular.element(window).width() - angular.element(".loader").width() ) / 2 ) + "px");
        angular.element(".loader").css("top", ( ( angular.element(window).height()  - angular.element(".loader").height() ) / 2 ) + "px");

        var temp = angular.element(".e-loader ul li");

        var count = 0;
          
        if( temp.length > 0){
          for(var i = 0; i < temp.length; i++)
          {

            loadImage("/assets/images/" + temp.eq(i).html().trim(),function(){
              
              count++;

              if(count >= temp.length)
              {
                start();
              }
            
            });
            
          }
        }else{
          start();
        }
      }


      function start(){

        angular.element(".slide.selected").css('display','block');
        angular.element(".loader").css('display','none');

      }

    }, 0);
    
  }
  
});