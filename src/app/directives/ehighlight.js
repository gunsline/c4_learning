angular.module('eLearning')
  .directive('eHighlight', function ($timeout, utils, $window) {
  
  return function(scope, element, attr){
    
    var x = attr.hX;
    var y = attr.hY;
    var width = attr.hWidth;
    var height = attr.hHeight;
    var width_image = attr.hWidthImage;
    var height_image = attr.hHeightImage;
    var image_height;

    angular.element( window ).resize(function() {
      add();
    });

    add();
    
    
    function add(){

      element.css({
        "left":calc_x() + "%",
        "top":calc_y() + "%",
        "width":calc_width() + "%",
        "height":calc_height() + "%"
      });
    }

    function calc_diference(){
      var window_h = $window.innerHeight;

      image_height = ($window.innerWidth / (width_image / 100) ) * (height_image / 100);
      
      if(image_height > window_h) { return 100 };

      var total = 100 - ( ( window_h - image_height ) / ( window_h / 100 ) );

      return utils.nil(total) ? total : 100;
    }

    function calc_x(){
      return ( 100 * x ) / width_image;
    }

    function calc_y(){
      return ( ( calc_diference() * y ) / height_image );
    }

    function calc_width(){
      return ( 100 * width ) / width_image;
    }

    function calc_height(){
      return ( 100 * height ) / height_image;
    }

  }
  
});