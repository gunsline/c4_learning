#Apresentação

##e-background slide-style

``` 
- type: loader
- descriptions: ["1.jpg","2.jpg", "3.jpg", "4.jpg","5.jpg","6.jpg","7.jpg","8.jpg","9.jpg","img1.jpg","img2.png","logo.png","logo-c4.png","teste.jpg"]
```

``` 
- type: animation
- mode: sides
```

``` 
- type: block
- class: content_block_text1
- description: Crie cursos EAD
- animation: from_left
- position: [15, 20]
- delay: 1
- time: 1
- rotation: 180
```

``` 
- type: block
- class: content_block_text2
- description: Focando
- animation: from_left
- position: [15, 28]
- delay: 1.3
- time: 1
- rotation: 180
```

``` 
- type: block
- class: content_block_text3
- description: no design de conteúdo
- animation: from_left
- position: [15, 50]
- delay: 1.6
- time: 1
- rotation: 180
```
``` 
- type: block
- class: content_block_text3
- description: e pedagogia de ensino
- animation: from_left
- position: [15, 58]
- delay: 1.6
- time: 1
- rotation: 180
```

``` 
- type: block
- class: content_block_text4
- description: por Cassiano C. Casagrande
- animation: from_left
- position: [15, 75]
- delay: 2.2
- time: 1
- rotation: 180
```

``` 
- type: block
- class: content_block_hand
- image: hand.png
- animation: zoom
- zoom: 1.1
- position: [60, 20]
- delay: 2.5
- rotation: 360
```

Lorin lipsum dolor, iset fan teh teste 1

#Apresentação

##e-background slide-style2

``` 
- type: animation
- mode: sides
```

``` 
- type: block
- class: content_block_text1
- description: Resumo da ópera:
- animation: from_left
- position: [10, 20]
- delay: 1
- time: 1
```

``` 
- type: block
- class: content_block_text5
- description: > Projetando cursos EAD
- animation: from_left
- position: [10, 35]
- delay: 1.3
- time: 1
```

``` 
- type: block
- class: content_block_text5
- description: > Estimando os projetos
- animation: from_left
- position: [10, 45]
- delay: 1.6
- time: 1
```
``` 
- type: block
- class: content_block_text5
- description: > Mais tempo para agregar valor
- animation: from_left
- position: [10, 55]
- delay: 1.9
- time: 1
```

``` 
- type: block
- class: content_block_text5
- description: > Demonstrações C4
- animation: from_left
- position: [10, 65]
- delay: 2.2
- time: 1
```

``` 
- type: block
- class: content_block_image
- animation: from_right
- position: [50, 0]
- delay: 2.5
```

Lorin lipsum dolor, iset fan teh teste 2

#Projetando cursos EAD

##e-background slide-style3

``` 
- type: animation
- mode: sides
```

``` 
- type: block
- class: content_block_text7
- description: Cada curso com sua complexidade
- animation: from_left
- position: [10, 20]
- delay: 1
- time: 1
```

``` 
- type: block
- class: content_block_text5
- description: > Animações customizadas
- animation: from_left
- position: [10, 35]
- delay: 1.3
- time: 1
```

``` 
- type: block
- class: content_block_text5
- description: > Estimando os projetos
- animation: from_left
- position: [10, 45]
- delay: 1.6
- time: 1
```

``` 
- type: block
- class: content_block_text5
- description: > Diferentes tecnologias
- animation: from_left
- position: [10, 65]
- delay: 2.2
- time: 1
```

``` 
- type: block
- class: content_block_text5
- description: > Diferentes abordagens pedagógicas
- animation: from_left
- position: [10, 55]
- delay: 1.9
- time: 1
```

Lorin lipsum dolor, iset fan teh teste 2

#Projetando cursos EAD

##e-background slide-style3

``` 
- type: block
- class: content_block_text7
- description: Cada curso com sua complexidade
- animation: from_left
- position: [15, 20]
- delay: 1
- time: 1
- rotation: 180
```

``` 
- type: tip
- title: Riscos
- description: Projetos de EAD tem alto grau de variações e com isso os riscos de projeto também aumentam.
- position: [15, 50]
- animation: scale
- delay: 1.2
- time: 2
```

``` 
- type: tip
- title: Produtividade
- description: Se o ritmo de produtividade (tanto no design instrucional quanto no desenvolvimento técnico) não puder ser estimado com precisão, a gestão do projeto fica mais complexa.
- position: [35, 50]
- animation: scale
- delay: 1.3
- time: 2
```

``` 
- type: tip
- title: Foco
- description: Quem tem como missão transformar o conteúdo do cliente em um curso de qualidade, precisa focar ao máximo no design instrucional e na comunicação com o cliente.
- position: [55, 50]
- animation: scale
- delay: 1.4
- time: 2
```

``` 
- type: tip
- title: Qualidade
- description: Sem produtividade garantida, riscos controlados e tempo para focar no cliente a qualidade cai e tarefas extras consomem orçamento.
- position: [75, 50]
- animation: scale
- delay: 1.4
- time: 2
```

``` 
- type: animation
- mode: sides
```


Lorin lipsum dolor, iset fan teh teste 2

#Estimando projetos

##e-background slide-style2

``` 
- type: tab
- titles: ["Design instrucional", "Implementação técnica"]
- descriptions: ["> Semelhante ao levantamento de requisitos um projeto de software. Porém com foco em filtrar, organizar e formatar conteúdos.<br />> Como a maior parte dos conteúdos se apresenta em linguagem natural (textos), não existe uma forma simples de realizar este trabalho.<br/>> Depende da experiência do profissional envolvido.","> Seja qual for a tecnologia, HTML5 ou Flash, possuir bibliotecas de componentes customizáveis (framework) permite maior previsibilidade.<br />> Um demo de todos os componentes interativos facilita a aprovação e padronização pré desenvolvimento visual de design e funcionamento.<br />> Controle de qualidade através de checklists pré definidos e aprovados juntamente com o cliente."]
```

``` 
- type: animation
- mode: sides
```


Lorin lipsum dolor, iset fan teh teste 2


#Design instrucional

##e-background slide-style2

``` 
- type: block
- class: content_block_text6
- description: Nesta fase o ideal direcionar as forças no que agrega mais valor: <strong>foque no roteiro</strong>.
- animation: from_left
- position: [15, 20]
- delay: 1
- time: 1
- rotation: 180
```

``` 
- type: tip
- title: Escopo pré demo
- description: Todo escopo do que será customizado e criado para o projeto de curso deve ser alinhado previamente com cliente o mais rápido possível.
- position: [15, 50]
- animation: scale
- delay: 1.2
- time: 2
```

``` 
- type: tip
- title: Roteiro demo
- description: Toda didática, design, componentes interativos e áudio visuais a serem usados. Padrões textuais e demais padrões também devem ser definidos pelo designer instrucional e aprovados pelo cliente nesta etapa. 
- position: [35, 50]
- animation: scale
- delay: 1.3
- time: 2
```

``` 
- type: tip
- title: Checklist roteiro
- description: Um checklist para controlar a qualidade do roteiro e garantir que os padrões acordados também com a equipe técnica sejam seguidos. Por exemplo: o conteúdo entregue está completo? e segue todos padrões alinhados?
- position: [55, 50]
- animation: scale
- delay: 1.4
- time: 2
```

``` 
- type: animation
- mode: sides
```


Lorin lipsum dolor, iset fan teh teste 2


#Titulo E-learning 7

##e-background slide-style3

``` 
- type: block
- class: content_block_text7
- description: Diminua e controle riscos: nesta fase, <strong>não seja surpreendido.</strong>
- animation: from_left
- position: [15, 20]
- delay: 1
- time: 1
- rotation: 180
```

``` 
- type: tip
- title: Domine a tecnologia
- description: Para que a produtividade seja uniforme e o cronograma seja cumprido com qualidade, tenho certeza que sua equipe técnica domina as tecnologias utilizadas. Equipe precisa estar em constante evolução.
- position: [15, 50]
- animation: scale
- delay: 1.2
- time: 2
```

``` 
- type: tip
- title: Terceirize
- description: Tenha um fornecedor de implementações para cursos de sua confiança sempre por perto. Assim a qualidade e produtividade ficam garantidas e sua empresa pode focar mais no negócio que ela domina.
- position: [35, 50]
- animation: scale
- delay: 1.3
- time: 2
```

``` 
- type: tip
- title: Checklist curso final
- description: Use a implementação do roteiro demo para montar um checklist de qualidade para os cursos implementados. Ele será usado para validar, por exemplo: padrões de design, funcionais, se os textos estão de acordo, etc.
- position: [55, 50]
- animation: scale
- delay: 1.4
- time: 2
```

``` 
- type: animation
- mode: sides
```


Lorin lipsum dolor, iset fan teh teste 2

#Titulo E-learning 8

##e-background slide-style3

``` 
- type: block
- class: content_block_text9
- description: A C4 INTERNET pode ajudar
- animation: from_left
- position: [10, 10]
- delay: 0.6
- time: 1
```

``` 
- type: block
- class: content_block_text8
- description: Fábrica de implementação:
- animation: from_left
- position: [10, 30]
- delay: 1
- time: 1
```

``` 
- type: block
- class: content_block_text5
- description: > Crie seu roteiro como quiser
- animation: from_left
- position: [10, 45]
- delay: 1.3
- time: 1
```

``` 
- type: block
- class: content_block_text5
- description: > Demos criados em alta velocidade: HTML5 ou Flash
- animation: from_left
- position: [10, 55]
- delay: 1.6
- time: 1
```

``` 
- type: block
- class: content_block_text5
- description: > Ambiente online para designers instrucionais
- animation: from_left
- position: [10, 65]
- delay: 1.9
- time: 1
```

``` 
- type: block
- class: content_block_text5
- description: > Processo de trabalho validado
- animation: from_left
- position: [10, 75]
- delay: 2.2
- time: 1
```


``` 
- type: block
- class: content_block_text5
- description: > Produtividade escalável e controlada
- animation: from_left
- position: [10, 85]
- delay: 2.5
- time: 1
```

``` 
- type: animation
- mode: sides
```


Lorin lipsum dolor, iset fan teh teste 2

#Titulo E-learning 8

##e-background slide-style4

``` 
- type: block
- class: content_block_text9
- description: Criando tarefas com JIRA
- animation: from_left
- position: [10, 10]
- delay: 0.6
- time: 1
```

``` 
- type: block
- class: content_block_text12
- description: Atlanssian JIRA é uma ferramenta para gestão de projetos que possui uma excelente relação custo versus benefício. Vamos utilizar esta ferramenta para este demonstrativo.
- animation: from_left
- position: [10, 30]
- delay: 0.9
- time: 1
```

``` 
- type: block
- class: content_block_text11
- description: Pré-requisitos para este curso demo:<ul><li>possuir uma conta no JIRA Cloud ou ter instalado JIRA em um servidor próprio</li><li>possuir um usuário com permissão para criar tarefas</li><li>estar dentro do sistema</li><li>ter criado um projeto previamente</li></ul>
- animation: from_left
- position: [10, 50]
- delay: 1.2
- time: 1
```

``` 
- type: animation
- mode: sides
```


Lorin lipsum dolor, iset fan teh teste 2

#Jira 1

![alt opcional](1.jpg "tittle")

``` 
- type: animation
- mode: sides
```

```
- type: highlight
- size: [80, 35]
- position: [437, 0]
```

``` 
- type: block
- class: content_block_text10
- description: Esta é a “Dashboard” da ferramenta Atlanssian JIRA. Par visualiza-la você precisará estar logado no sistema.<br><br>Para iniciar a criação de uma tarefa clique no botão verde “Create” destacada logo acima.
- animation: from_right
- position: [45, 20]
- delay: 1
- time: 0.7
```

Jira 1

#Jira 2

 

```
- type: highlight
- size: [172, 37]
- position: [930, 255]
```

![alt opcional](2.jpg "tittle")

Jira 2

#Jira 3

 

```
- type: highlight
- size: [100, 25]
- position: [940, 345]
- animation: show
- delay: 1
```

``` 
- type: block
- class: content_block_text10
- description: Agora marque as seguintes opções listadas:<ul><li>“Assignee”, pessoa que fará a tarefa</li><li>“Description”, dicas do que será feito</li><li>“Priority”, prioridade que a tarefa terá</li></ul>
- animation: from_left
- position: [15, 20]
- delay: 1
- time: 1
```

``` 
- type: block
- class: content_block_popup
- description: 
- animation: scale
- position: [65, 38]
- delay: 0.3
- time: 1
```

![alt opcional](2.jpg "tittle")

Jira 3

#Jira 4

 

```
- type: highlight
- size: [110, 25]
- position: [940, 447]
```

``` 
- type: block
- class: content_block_text10
- description: Agora marque as seguintes opções listadas:<ul><li>“Assignee”, pessoa que fará a tarefa</li><li>“Description”, dicas do que será feito</li><li>“Priority”, prioridade que a tarefa terá</li></ul>
- position: [15, 20]
```

``` 
- type: block
- class: content_block_popup2
- description: 
- position: [65, 38]
```

![alt opcional](4.jpg "tittle")

Jira 4

#Jira 5

 

![alt opcional](5.jpg "tittle")

```
- type: highlight
- size: [80, 25]
- position: [1175, 417]
```

``` 
- type: block
- class: content_block_text10
- description: Agora marque as seguintes opções listadas:<ul><li>“Assignee”, pessoa que fará a tarefa</li><li>“Description”, dicas do que será feito</li><li>“Priority”, prioridade que a tarefa terá</li></ul>
- position: [15, 20]
```

``` 
- type: block
- class: content_block_popup3
- description: 
- position: [65, 38]
```


Jira 5

#Jira 6

 

![alt opcional](6.jpg "tittle")

``` 
- type: block
- class: content_block_popup4
- description: 
- position: [65, 38]
```

``` 
- type: block
- class: content_block_text10
- description: Agora marque as seguintes opções listadas:<ul><li>“Assignee”, pessoa que fará a tarefa</li><li>“Description”, dicas do que será feito</li><li>“Priority”, prioridade que a tarefa terá</li></ul>
- position: [15, 20]
```

Jira 6

#Jira 7

```
- type: block
- class: content_block_text10
- description: Agora marque digite Passar
- position: [15, 20]
```

```
- type: input_text
- size: [496, 27.2]
- position: [475, 288]
- word: Passar
```

![alt opcional](7.jpg "tittle")

Jira 7

#Jira 8


![alt opcional](8.jpg "tittle")

Jira 8

#Jira 9

 

``` 
- type: block
- class: content_block_text10
- description: Pronto! Sua tarefa, também chamada de issue ou pendência, foi criada com sucesso.<br><br>Agora vamos testar seus conhecimentos, passe para próxima página!
- animation: from_left
- position: [25, 20]
- delay: 1
- time: 1
```

![alt opcional](9.jpg "tittle")

Jira 9


#Amostra de implementação: criando tarefas

##e-background slide-style3


``` 
- type: block
- class: content_block_text9
- description: Exercício de fixação
- animation: from_left
- position: [10, 10]
- delay: 0.6
- time: 1
```

``` 
- type: block
- class: content_block_text14
- description: Marque a opção correta. São apenas 2 perguntas.  ;)
- animation: from_left
- position: [10, 20]
- delay: 0.9
- time: 1
```

``` 
- type: block
- class: content_block_text11
- description: 2- O que é uma issue na ferramenta JIRA?
- animation: from_left
- position: [10, 40]
- delay: 1.2
- time: 1
```

``` 
- type: block
- class: content_block_text11
- description: a- uma tarefa<br>b- uma pendencia<br>c- todas as anteriores
- animation: from_left
- position: [12, 45]
- delay: 1.2
- time: 1
```

```
- type: input_radio
- position: [135, 345]
- value: uma pendencia
- labels: ["uma tarefa", "uma pendencia", "todas as anteriores"]
- animation: scale
- delay: 1.5
```

``` 
- type: block
- class: content_block_imagem2
- description: 
- animation: from_right
- position: [55, 40]
- delay: 1.2
- time: 1
- rotation: 180
```

``` 
- type: animation
- mode: sides
```

Lorin lipsum dolor, iset fan teh teste 2

#Titulo E-learning 8

##e-background slide-style

``` 
- type: block
- class: content_block_text13
- description: Exercício de fixação
- animation: from_left
- position: [10, 10]
- delay: 0.6
- time: 1
```

``` 
- type: block
- class: content_block_text14
- description: Marque a opção correta. São apenas 2 perguntas.  ;)
- animation: from_left
- position: [10, 20]
- delay: 0.9
- time: 1
```

``` 
- type: block
- class: content_block_text11
- description: 1- O que é Atlanssia JIRA?
- animation: from_left
- position: [10, 40]
- delay: 1.2
- time: 1
```

``` 
- type: block
- class: content_block_text11
- description: a- um aplicativo para celular<br>b- uma ferramenta de edição de cursos<br>c- uma ferramenta de gerencia de projetos
- animation: from_left
- position: [12, 45]
- delay: 1.2
- time: 1
```

```
- type: input_radio
- position: [135, 345]
- value: uma ferramenta de gerencia de projetos
- labels: ["um aplicativo para celular", "uma ferramenta de edição de cursos", "uma ferramenta de gerencia de projetos"]
- animation: scale
- delay: 1.5
```

``` 
- type: block
- class: content_block_imagem1
- description: 
- animation: from_right
- position: [55, 40]
- delay: 1.2
- time: 1
- rotation: 180
```

``` 
- type: animation
- mode: sides
```

Lorin lipsum dolor, iset fan teh teste 2

#Encerramento

##e-background slide-style4

``` 
- type: block
- class: content_block_text9
- description: Apresentação concluída com sucesso.<br>Parabéns! 
- animation: from_left
- position: [10, 10]
- delay: 0.6
- time: 1
```

``` 
- type: block
- class: content_block_text14
- description: Para mais informações entre em contato pelo email:
- animation: from_left
- position: [10, 45]
- delay: 0.9
- time: 1
```

``` 
- type: block
- class: content_block_text11
- description: <a href="mailTo:contato@c4internet.com.br">contato@c4internet.com.br</a> <br />Ou acesse: <br /> <a target="_blank" href="http://www.c4internet.com.br">www.c4internet.com.br</a>
- animation: from_left
- position: [10, 60]
- delay: 1.2
- time: 1
```

``` 
- type: block
- class: content_block_imagem3
- description: 
- animation: from_right
- position: [75, 60]
- delay: 1.2
- time: 1
- rotation: 180
```

``` 
- type: animation
- mode: sides
```

Lorin lipsum dolor, iset fan teh teste 2